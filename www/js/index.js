var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        // cordova plugin add cordova-plugin-device-motion
        // これで加速度センサーのプラグインを有効化
        console.log(navigator.accelerometer);
        drawLoopSquare();
    }
};

app.initialize();

function drawLoopSquare() {
    setInterval(function () {
        // 今の端末にかかっている加速度をキャッチする関数
        // コールバック関数には計測結果が代入される
        // 面倒だから省略したけど第二引数にはエラー用のコールバック関数が入る
        navigator.accelerometer.getCurrentAcceleration(function (acceleration) {
            // canvasを見つける
            var cs  = document.getElementById('mainCanvas');
            var ctx = cs.getContext('2d');
            // x方向とy方向の加速度を取得
            var x = acceleration.x * -10 + 100;
            var y = acceleration.y * 10 + 100;
            // canvasを一度まっさらに
            ctx.clearRect(0, 0, cs.width, cs.height);
            // 描画開始
            ctx.beginPath();
            // 縦線
            ctx.moveTo(100,   0);
            ctx.lineTo(100, 200);
            ctx.stroke();
            // 横線
            ctx.moveTo(  0, 100);
            ctx.lineTo(200, 100);
            ctx.stroke();
            // カーソル
            ctx.strokeRect(x, y, 10, 10);
        });
    }, 100)/* <- 0.1秒おきに実行 多分そんなスピードで動けてない */
};

